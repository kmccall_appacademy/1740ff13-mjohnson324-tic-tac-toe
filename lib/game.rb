require_relative 'board'
require_relative 'human_player'
require_relative 'computer_player'

class Game
  def initialize(player_one, player_two)
    @player_one = player_one
    @player_two = player_two
    @board = Board.new
    @switch = 1
  end

  attr_reader :player_one, :player_two, :board, :switch

  def play_turn
    player_choice = @player_one.get_move
    @board.place_mark(player_choice, :X)
    switch_players!
  end

  def current_player
    if @switch == 1
      @player_one
    else
      @player_two
    end
  end

  def switch_players!
    if @switch == 1
      @switch = 2
    else
      @switch = 1
    end
  end
end
