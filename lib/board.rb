class Board
  def initialize(grid = [ [nil, nil, nil], [nil, nil, nil], [nil, nil, nil] ])
    @grid = grid
  end

  attr_accessor :grid

  def place_mark(position, mark)
    raise "Please enter :X or :O" unless mark == :X || mark == :O
    row = position[0]
    column = position[1]
    @grid[row][column] = mark
  end

  def empty?(position)
    row = position[0]
    column = position[1]
    return true unless @grid[row][column].is_a? Symbol
    false
  end

  def row_check
    @grid.each do |row|
      symbol = row[0]
      count = 0
      row.each do |mark|
        count += 1 if mark == symbol && !symbol.nil?
      end
      return symbol if count == @grid.length
    end
    nil
  end

  def column_check
    column = 0
    while column < @grid.length
      row = 0
      count = 0
      symbol = @grid[row][column]
      while row < @grid.length
        mark = @grid[row][column]
        count += 1 if mark == symbol && !symbol.nil?
        row += 1
      end
      return symbol if count == @grid.length
      column += 1
    end

    nil
  end

  def left_diagonal_check
    symbol = @grid[0][0]
    count = 0
    @grid.each_with_index do |r_ow, idx|
      mark = r_ow[idx]
      count += 1 if mark == symbol && !symbol.nil?
    end
    return symbol if count == @grid.length
    nil
  end

  def right_diagonal_check
    symbol = @grid[0][-1]
    count = 0
    @grid.reverse_each.with_index do |r_ow, idx|
      mark = r_ow[idx]
      count += 1 if mark == symbol && !symbol.nil?
    end
    return symbol if count == @grid.length
    nil
  end

  def winner
    possible_winners = [row_check, column_check,
                        left_diagonal_check, right_diagonal_check]

    possible_winners.each do |victor|
      return victor if victor
    end
    nil
  end

  def over?
    full_grid_check = @grid.flatten
    return true if winner || full_grid_check == full_grid_check.compact
    false
  end
end
