class ComputerPlayer
  def initialize(name)
    @name = name
  end

  attr_accessor :name, :mark, :board

  def get_move
    @board.grid.each_with_index do |row, row_num|
      row.each_index do |column|
        return [row_num, column] if winning_move?([row_num, column])
      end
    end
    random_move
  end

  def winning_move?(position)
    row = position[0]
    column = position[1]
    unless @board.grid[row][column].is_a? Symbol
      @board.place_mark([row, column], :O)
      victory = @board.winner
      @board.grid[row][column] = nil
      return victory if victory
    end
    false
  end

  def random_move
    last_idx = @board.grid.length - 1
    rand_num = (1..last_idx).to_a

    move_made = false
    until move_made == true
      row = rand_num.shuffle[0]
      column = rand_num.shuffle[0]
      unless @board.grid[row][column].is_a? Symbol
        move_made = true
      end
    end

    [row, column]
  end

  def display(board)
    @board = board
  end
end
