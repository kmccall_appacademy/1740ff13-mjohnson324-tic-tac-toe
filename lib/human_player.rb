class HumanPlayer
  def initialize(name)
    @name = name
  end

  attr_accessor :name, :mark, :board

  def get_move
    puts "Choose where to lay a mark:"
    position = gets.chomp
    row = position[0].to_i
    column = position[-1].to_i
    [row, column]
  end

  def display(board)
    board.grid.each do |row|
      print "#{row}\n"
    end
  end
end
